import java.util.Scanner;
class TicTacToeApp {
	public static void main(String args []) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to Tic Tac Toe\nplayer's token: X\nplayer's 2 taken: O");
		Board b1 = new Board();
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		while(!gameOver) {
			System.out.println(b1);
			System.out.println("Player " + player + ": it's your turn where do you want to place yout token?");
			int row = reader.nextInt();
			int col = reader.nextInt();
			if(player == 1) {
				playerToken = Tile.X;
			}else {
				playerToken = Tile.O;
			}
			boolean played = b1.placeToken(row, col, playerToken);
			while(!played) {
				System.out.println("Please enter a vaild number");
				row = reader.nextInt();
				col = reader.nextInt();
				played = b1.placeToken(row, col, playerToken);
			}
			if(b1.checkIfWinning(playerToken)) {
				System.out.println("Player " + player + " wins\n" + b1);
				gameOver = true;
			}else if(b1.checkIfFull()) {
				System.out.println("It's a tie, nobody wins\n" + b1);
				gameOver = true;
			}else {
				player++;
				if(player>2) {
					player = 1;
				}
			}
			
		}
	}
}