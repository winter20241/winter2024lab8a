class Board {
	private Tile[][] grid;
	
	public Board() {
		this.grid = new Tile [][] {{Tile.BLANK, Tile.BLANK, Tile.BLANK},{Tile.BLANK, Tile.BLANK, Tile.BLANK},{Tile.BLANK, Tile.BLANK, Tile.BLANK}};
	}
	
	public String toString() {
		String print = "  0  1  2\n";
		for(int i = 0; i<grid.length; i++) {
			print = print + i + " ";
			for(int j = 0; j<grid[i].length; j++) {
				if(grid[i][j] == Tile.BLANK) {
					print = print + "_" + "  ";
				}else {
					print = print + grid[i][j] + "  ";
				}
			}
			print = print + "\n";
		}
		return print;
	}
	public boolean placeToken(int row, int col, Tile playerToken) {
		if(row <= 2 && col <= 2) {
			if(grid[row][col] == Tile.BLANK) {
				grid[row][col] = playerToken;
				return true;
			}
			else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	public boolean checkIfFull() {
		boolean check = true;
		for(int i = 0; i<grid.length; i++) {
			for(int j = 0; j<grid[i].length; j++) {
				if(grid[i][j]== Tile.BLANK){
					return false;
				}
			}
		}
		return check;
	}
	
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		int checkCount = 0;
		for(int i = 0; i<grid.length; i++) {
			for(int j = 0; j<grid[i].length; j++) {
				if(playerToken == grid[i][j]){
					checkCount++;
				}
			}
			if(checkCount == 3) {
				return true;
			}else {
				checkCount = 0;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Tile playerToken) {
		int checkCount = 0; 
		for(int i = 0; i<grid.length; i++) {
			for(int j = 0; j<grid[i].length; j++) {
				if(playerToken == grid[j][i]) {
					checkCount++;
				}
			}
			if(checkCount == 3){
				return true;
			}else {
				checkCount = 0;
			}
		}
		return false;
	}
	
	public boolean checkIfWinning(Tile playerToken) {
		if(checkIfWinningHorizontal(playerToken)){
			return true;
		}else if(checkIfWinningVertical(playerToken)) {
			return true;
		}else {
			return false;
		}
	}
}